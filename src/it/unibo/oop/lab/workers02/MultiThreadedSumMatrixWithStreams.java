package it.unibo.oop.lab.workers02;

import java.util.stream.IntStream;

public class MultiThreadedSumMatrixWithStreams implements SumMatrix {
	
	private final int nThread;
	
	/**
     * Construct a multithreaded matrix sum.
     * 
     * @param nthread
     *            no. threads to be adopted to perform the operation
     */
    public MultiThreadedSumMatrixWithStreams(final int nthread) {
        super();
        if (nthread < 1) {
            throw new IllegalArgumentException();
        }
        this.nThread = nthread;
    }
    
	@Override
	public double sum(final double[][] matrix) {
		final int size = matrix.length / nThread + matrix.length % nThread;
		/*
		 * parallel pipeline processing
		 */
		return IntStream.iterate(0, start -> start + size)
				.limit(nThread)
				/*
                 * We do not create thread ourselves. We decide how tasks should be done, and we
                 * ask the Stream library to spawn the required threads.
                 */
				.parallel()
				.mapToDouble(start -> {
					double result = 0;
					for (int i = start; i < matrix.length && i < start + size; i++) {
						for (final double d : matrix[i]) {
							result += d;
						}
					}
					return result;
				})
				.sum();
	}

}
